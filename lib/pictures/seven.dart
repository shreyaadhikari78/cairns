import 'package:flutter/material.dart';
import 'package:museum/Screens/stops/stopSeven.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class Seven extends StatefulWidget {
  @override
  _SevenState createState() => _SevenState();
}

class _SevenState extends State<Seven> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StopSeven()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/STOP 7.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

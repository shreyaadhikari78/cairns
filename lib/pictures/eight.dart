import 'package:flutter/material.dart';
import 'package:museum/Screens/stops/stopEight.dart';
import 'package:museum/Screens/stops/stopSeven.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class Eight extends StatefulWidget {
  @override
  _EightState createState() => _EightState();
}

class _EightState extends State<Eight> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StopEight()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/STOP 8.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

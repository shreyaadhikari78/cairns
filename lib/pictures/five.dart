import 'package:flutter/material.dart';
import 'package:museum/Screens/informationPage.dart';
import 'package:museum/Screens/stops/stopFive.dart';
import 'package:museum/Screens/stops/stopFour.dart';
import 'package:museum/Screens/stops/stopOne.dart';
import 'package:museum/Screens/stops/stopThree.dart';
import 'package:museum/Screens/stops/stopTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class Five extends StatefulWidget {
  @override
  _FiveState createState() => _FiveState();
}

class _FiveState extends State<Five> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StopFive()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/STOP 5.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

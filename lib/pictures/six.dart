import 'package:flutter/material.dart';
import 'package:museum/Screens/stops/stopSix.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class Six extends StatefulWidget {
  @override
  _SixState createState() => _SixState();
}

class _SixState extends State<Six> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StopSix()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/STOP 6.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

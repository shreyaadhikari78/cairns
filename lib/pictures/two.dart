import 'package:flutter/material.dart';
import 'package:museum/Screens/informationPage.dart';
import 'package:museum/Screens/stops/stopOne.dart';
import 'package:museum/Screens/stops/stopThree.dart';
import 'package:museum/Screens/stops/stopTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class Two extends StatefulWidget {
  @override
  _TwoState createState() => _TwoState();
}

class _TwoState extends State<Two> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StopTwo()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/STOP 2.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

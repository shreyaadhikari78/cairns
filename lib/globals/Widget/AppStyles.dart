import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';



final titleStyle = TextStyle(color:Colors.black, fontSize: 25.0.sp, fontFamily: 'JosefinSans', fontStyle: FontStyle.normal, fontWeight: FontWeight.w500);
final infostyle = TextStyle(color: Colors.black, fontSize: 15.0.sp, fontWeight: FontWeight.w600 );
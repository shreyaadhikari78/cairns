import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/Screens/mapPage.dart';
import 'package:museum/Screens/thankYou.dart';
import 'package:museum/Screens/stops/stopFive.dart';
import 'package:museum/Screens/stops/stopFour.dart';
import 'package:museum/Screens/stops/stopThree.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:museum/pictures/five.dart';

import 'AppColors.dart';
Widget showActivityFourAlert(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: true,
      builder: (dialogContext) {
        return AlertDialog(
          backgroundColor: AppColors.primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Text("$message", style: infostyle ,),
          actions: <Widget>[
            FlatButton(
                child: Text("Ok"),
                textColor:  AppColors.blueColor,
                onPressed: () {
                  Navigator.pop(context);
                }),
          ],
        );
      });
}
Widget showCorrectActivityFourAnswer(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext eventContext) {
        return AlertDialog(
          backgroundColor: AppColors.primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Text("$message", style: infostyle ,),
          actions: <Widget>[
            Row(
              children: [
                FlatButton(
                    child: Text("Go to Next Activity"),
                    textColor:  AppColors.blueColor,
                    onPressed: () {
                      Navigator.pushReplacement(
                          eventContext,
                          CupertinoPageRoute(
                              builder: (BuildContext context) =>Five()));
                    }),
                FlatButton(
                    child: Text("Cancel"),
                    textColor:  AppColors.blueColor,
                    onPressed: () {
                      Navigator.pop(eventContext);
                    }),
              ],
            ),
          ],
        );
      });
}
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/globals/Widget/ActivityOneAlertDailouge.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';

import 'package:sizer/sizer.dart';

import '../mapPage.dart';

class ActivityOne extends StatefulWidget {
  String answer = "1907";
  bool goToNext =false;

  @override
  _ActivityOneState createState() => _ActivityOneState();
}

class _ActivityOneState extends State<ActivityOne> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String finalFirstAnswer;
    TextEditingController firstAnswerController = TextEditingController();
    return Scaffold(

      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      body: Stack(children: <Widget>[
        Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/background.jpg"),
                  fit: BoxFit.fitHeight)),
        ),
        Center(
          child: Container(
              width: 80.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    Text(
                      "When was the School of Arts built?",
                      style: infostyle,
                    ),
                    SizedBox(
                      height: 1.0.h,
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextFormField(
                            controller: firstAnswerController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'this field cannot be left empty';
                              }
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Center(
                              child: MaterialButton(
                                color: Colors.greenAccent,
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    widget.answer ==
                                            firstAnswerController.value.text
                                                .trim()
                                        ? showCorrectActivityOneAnswer(context,"Answer is Correct")
                                        : showActivityOneAlert(context,"Answer is Incorrect");
                                  }
                                },
                                child: Text('Submit'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
              )),
        ),
      ]),
    );

  }

}

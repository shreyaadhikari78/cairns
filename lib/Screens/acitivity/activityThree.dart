import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/globals/Widget/ActivityOneAlertDailouge.dart';
import 'package:museum/globals/Widget/ActivityThreeAlertDialouge.dart';
import 'package:museum/globals/Widget/ActivityTwoAlertDailouge.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';

import 'package:sizer/sizer.dart';

import '../mapPage.dart';

class ActivityThree extends StatefulWidget {
  String answer = "Lift";
  bool goToNext =false;

  @override
  _ActivityThreeState createState() => _ActivityThreeState();
}

class _ActivityThreeState extends State<ActivityThree> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String finalFirstAnswer;
    TextEditingController firstAnswerController = TextEditingController();
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),

      backgroundColor: AppColors.primaryColor,
      body: Stack(children: <Widget>[
        Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/background.jpg"),
                  fit: BoxFit.fitHeight)),
        ),
        Center(
          child: Container(
              width: 80.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Text(
                          "The Boland's Centre had the first what in Cairns? ",
                          style: infostyle,
                        ),
                        SizedBox(
                          height: 1.0.h,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              TextFormField(
                                controller: firstAnswerController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'this field cannot be left empty';
                                  }
                                },
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 16.0),
                                child: Center(
                                  child: MaterialButton(
                                    color: Colors.greenAccent,
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        widget.answer ==
                                            firstAnswerController.value.text
                                                .trim()
                                            ? showCorrectActivityThreeAnswer(context,"Answer is Correct")
                                            : showActivityThreeAlert(context,"Answer is Incorrect");
                                      }
                                    },
                                    child: Text('Submit'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
              )),
        ),
      ]),
    );

  }

}

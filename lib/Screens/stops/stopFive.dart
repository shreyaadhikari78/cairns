import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityFive.dart';
import 'package:museum/Screens/acitivity/activityFour.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/Screens/acitivity/activityThree.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopFive extends StatefulWidget {
  @override
  _StopFiveState createState() => _StopFiveState();
}

class _StopFiveState extends State<StopFive> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityFive()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 5: Cairns Wharf  Fun Fact\n\n - This port was why the city of Cairns was established.\n\n "
                        "- The crane has been here since 1908.It used to unload railway wagons that came in on the tracks. The wagons contained timber, gold, fruit, and sugar, for export.",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}
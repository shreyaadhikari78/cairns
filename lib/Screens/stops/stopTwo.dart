import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopTwo extends StatefulWidget {
  @override
  _StopTwoState createState() => _StopTwoState();
}

class _StopTwoState extends State<StopTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityTwo()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 2 Hides Hotel: Fun Fact\n\n - Hides Hotel was first made from timber\n\n "
                        "-It was one of the first 3-storey buildings when it was built in 1930 \n\n-The story is that George Hides swapped a horse for the land!",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}

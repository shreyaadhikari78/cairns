import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopOne extends StatefulWidget {
  @override
  _StopOneState createState() => _StopOneState();
}

class _StopOneState extends State<StopOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityOne()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 1: School of Arts Fun Fact\n\n - This is the oldest public building in Cairns–it was built in 1907.\n\n -It wasn’t a school for artists but for learning–lectures were held here and a library was here too.\n\n "
                    "- In 1939 there was an extension added in a famous‘art deco’ style–can you find it?",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}

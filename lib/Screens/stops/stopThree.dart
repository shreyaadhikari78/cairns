import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/Screens/acitivity/activityThree.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopThree extends StatefulWidget {
  @override
  _StopThreeState createState() => _StopThreeState();
}

class _StopThreeState extends State<StopThree> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityThree()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 3 Boland's Centre: Fun Fact\n\n - This building was built in 1913–over 100 years ago–and was the first Department Store in Cairns. \n\n "
                        "- The cement was brought from England in boats–it was so heavy they had to anchor 12 km off-shore. \n\n-It had the first lift in Cairns–go in the door and look up!",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityEight.dart';
import 'package:museum/Screens/acitivity/activityFive.dart';
import 'package:museum/Screens/acitivity/activityFour.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/Screens/acitivity/activitySeven.dart';
import 'package:museum/Screens/acitivity/activitySix.dart';
import 'package:museum/Screens/acitivity/activityThree.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopEight extends StatefulWidget {
  @override
  _StopEightState createState() => _StopEightState();
}

class _StopEightState extends State<StopEight> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityEight()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 8: Cairns Lagoon Fun Fact\n\n - The Cairns lagoon is shaped like the state of Queensland.  \n\n "
                        "- The boulders at the pointy end represent the Torres Strait Islands \n\n -The fish at the other end were made by an artist from the Torres Strait Islands.",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/acitivity/activityFour.dart';
import 'package:museum/Screens/acitivity/activityOne.dart';
import 'package:museum/Screens/acitivity/activityThree.dart';
import 'package:museum/Screens/acitivity/activityTwo.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:sizer/sizer.dart';

class StopFour extends StatefulWidget {
  @override
  _StopFourState createState() => _StopFourState();
}

class _StopFourState extends State<StopFour> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ActivityFour()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/map.PNG"),
                    fit: BoxFit.cover))),
        Center(
          child: Container(
              width: 100.0.w,
              height: 60.0.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage("assets/images/scroll3.png"))),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 12.h, left: 22.w, right: 20.w, bottom: 12.h),
                child: SingleChildScrollView(
                  child: Text(
                    "STOP 4  Fun Fact\n\n - Lake Street was often flooded during the wet season in Cairns.\n\n "
                        "- People thought that’s why it was called Lake St but it was actually named after a ship’s captain–Captain Lake",
                    style: infostyle,
                  ),
                ),
              )),
        ),
      ]),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:museum/Screens/informationPage.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:sizer/sizer.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => InformationPage()),
          );
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: Stack(children: <Widget>[
        Container(
            constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/NAVIGATION.jpg"),
                    fit: BoxFit.cover))),
      ]),
    );
  }
}

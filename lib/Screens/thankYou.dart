import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';

class PhotoAlbum extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,

      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              
          ),
          child: Padding(
              padding: EdgeInsets.only(top: 9.0.h, left: 5.0.w, right: 5.0.w),
              child: Column(children: [
                Text(
                  "Congratulations!\n\nYou have crossed the finish line of the Cairns Kids Trail. Have a swim at the lagoon or head back to the Museum for some more historical fun facts and activities."
                      "\n\nVisit us soon!\n\nDon’t forget to rate us before you leave!",
                  style: titleStyle,
                ),


              ])),
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/mapPage.dart';
import 'package:museum/Screens/informationPage.dart';
import 'package:sizer/sizer.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
      Container(
      constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/home.jpg"),
                fit: BoxFit.cover)
        )),



        Positioned(bottom: 5.0.h,left: 20.0.w,
        child: GestureDetector(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MapPage()),
              );

            },
            child: Image.asset("assets/images/button.png")),
        ),
        Positioned(bottom: 5.0.h,left: 39.0.w,
          child: Text("Tap To Start"),
        ),

      ]),
    );
  }
}

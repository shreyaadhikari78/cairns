import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:museum/Screens/stops/stopOne.dart';
import 'package:museum/globals/Widget/AppColors.dart';
import 'package:museum/globals/Widget/AppStyles.dart';
import 'package:museum/pictures/one.dart';
import 'package:sizer/sizer.dart';


class InformationPage extends StatefulWidget {
  @override
  _InformationPageState createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.primaryColor,
        leading: new IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.arrow_back)),
      ),
      backgroundColor: AppColors.primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => One()),
          );

          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: AppColors.blueColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/scroll3.png")
              )
          ),
          child: Padding(
              padding: EdgeInsets.only(top: 9.0.h, left: 5.0.w, right: 5.0.w),
              child: Column(children: [
                Text(
                  "Information",
                  style: titleStyle,
                ),
                SizedBox(
                  height: 4.0.h,
                ),
                _infoText(),
                SizedBox(
                  height: 2.0.h,
                ),
                _imageWidget()
              ])),
        ),
      ),
    );
  }

  Widget _infoText() {
    return Container(
      height: 40.0.h,
      child: SingleChildScrollView(
        child: Text(
          "HELLO GUYS \nLet’s go on an adventure!\n\n"
              "-Walk to each location shown on the map.\n"
              "-Learn the interesting facts at each stop.\n"
              "-Answer the challenge question before moving to the next stop.\n\n"
              "-Have fun exploring!\n\n"
              "* Tour takes approximately 90 minutes to complete",
          style: infostyle,
        ),
      ),
    );
  }

  Widget _imageWidget() {
    return Container(
        height: 40.0.h,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    "assets/images/boy-girl-playing-in-water-illustration-.png"),
                fit: BoxFit.fill)));
  }
}
